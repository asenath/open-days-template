/*
* previewBoxStyle.backgroundColor = 'black' - zmiana koloru tła na czarny
* previewBoxStyle.rotate = '30deg' - obrót o 30 stopni
* previewBoxStyle.scale = '0.5' - zmniejszennie o połowę
* previewBoxStyle.boxShadow = '10px 10px 10px black' - cień przesunięty w prawo o 10px, w dół o 10px, rozmyty na 10px w kolorze czarnym
* previewBoxStyle.borderRadius = '50%' - zaokrąglenie krawędzi o 50% - kółko
* previewBoxStyle.border = '5px solid black' - obramowanie szerokości 5px w kolorze czarnym
*/

const previewBoxStyle = document.getElementById('preview-box').style
function funkcja() {
    previewBoxStyle.border = '5px solid black'
}